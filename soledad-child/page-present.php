<?php
/**
 * The template for displaying present pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Wordpress
 * @since   1.0
 */
get_header();
$breadcrumb      = get_post_meta( get_the_ID(), 'penci_page_breadcrumb', true );
$featured_boxes  = get_post_meta( get_the_ID(), 'penci_page_display_featured_boxes', true );
$page_meta       = get_post_meta( get_the_ID(), 'penci_page_slider', true );
$rev_shortcode   = get_post_meta( get_the_ID(), 'penci_page_rev_shortcode', true );
$sidebar_default = get_theme_mod( 'penci_page_default_template_layout' );

$sidebar_position = get_post_meta( get_the_ID(), 'penci_sidebar_page_pos', true );
if( $sidebar_position ) {
	$sidebar_default = $sidebar_position;
}
$class_small_width = '';
if( $sidebar_default == 'small-width' ):
	$class_small_width = ' penci-page-container-smaller';
endif;

/* Display Featured Boxes */
if ( $featured_boxes == 'yes' && ! get_theme_mod( 'penci_home_hide_boxes' ) && ( get_theme_mod( 'penci_home_box_img1' ) || get_theme_mod( 'penci_home_box_img2' ) || get_theme_mod( 'penci_home_box_img3' ) || get_theme_mod( 'penci_home_box_img4' ) ) ):
	get_template_part( 'inc/modules/home_boxes' );
endif;

?>
<?php $show_page_title = penci_is_pageheader(); ?>
<?php if( ! $show_page_title ): ?>
	<?php if ( ! get_theme_mod( 'penci_disable_breadcrumb' ) && ( 'no' != $breadcrumb ) && ! get_theme_mod( 'penci_move_breadcrumbs' ) ): ?>
		<?php
		$yoast_breadcrumb = '';
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			$yoast_breadcrumb = yoast_breadcrumb( '<div class="container container-single-page penci-breadcrumb ' . $class_small_width . '">', '</div>', false );
		}

		if ( $yoast_breadcrumb ) {
			echo $yoast_breadcrumb;
		} else { ?>
            <div class="container container-single-page penci-breadcrumb<?php echo $class_small_width; ?>">
		<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>
		<?php
		$page_parent = get_post_ancestors( get_the_ID() );
		if( ! empty( $page_parent ) ):
			$page_parent = array_reverse($page_parent);
			foreach( $page_parent as $pages ){
				?>
				<span><a class="crumb" href="<?php echo get_permalink( $pages ); ?>"><?php echo get_the_title( $pages ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>
			<?php }
		endif; ?>
                <span><?php the_title(); ?></span>
            </div>
		<?php } ?>
	<?php endif; ?>
<?php endif; ?>

    <div class="container container-single-page container-default-page<?php echo $class_small_width;
	if ( in_array( $sidebar_default, array( 'left-sidebar', 'right-sidebar', 'two-sidebar' ) ) ) {
		echo ' penci_sidebar ' . $sidebar_default;
	} else {
		echo ' penci_is_nosidebar';
	} ?>">
        <div id="main"
             class="penci-main-single-page-default <?php if ( get_theme_mod( 'penci_sidebar_sticky' ) ): ?> penci-main-sticky-sidebar<?php endif; ?>">
            <div class="theiaStickySidebar">

				<?php if ( ! $show_page_title ): ?>
					<?php if ( ! get_theme_mod( 'penci_disable_breadcrumb' ) && ( 'no' != $breadcrumb ) && get_theme_mod( 'penci_move_breadcrumbs' ) ): ?>
						<?php
						$yoast_breadcrumb = '';
						if ( function_exists( 'yoast_breadcrumb' ) ) {
							$yoast_breadcrumb = yoast_breadcrumb( '<div class="container container-single-page penci-breadcrumb penci-crumb-inside ' . $class_small_width . '">', '</div>', false );
						}

						if ( $yoast_breadcrumb ) {
							echo $yoast_breadcrumb;
						} else { ?>
                            <div class="container container-single-page penci-breadcrumb penci-crumb-inside<?php echo $class_small_width; ?>">
                                <span><a class="crumb"
                                         href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon( 'fas fa-angle-right' ); ?>
								<?php
								$page_parent = get_post_ancestors( get_the_ID() );
								if ( ! empty( $page_parent ) ):
									$page_parent = array_reverse( $page_parent );
									foreach ( $page_parent as $pages ) {
										?>
                                        <span><a class="crumb"
                                                 href="<?php echo get_permalink( $pages ); ?>"><?php echo get_the_title( $pages ); ?></a>
                                        </span><?php penci_fawesome_icon( 'fas fa-angle-right' ); ?>
									<?php }
								endif; ?>
                                <span><?php the_title(); ?></span>
                            </div>
						<?php } ?>
					<?php endif; ?>
				<?php endif; ?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
<style>
.gray-img {
-webkit-filter: grayscale(100%);
-moz-filter: grayscale(100%);
-ms-filter: grayscale(100%);
-o-filter: grayscale(100%);
filter: grayscale(100%);
}
.pagination{
   margin:40px 0 0;
   text-align:center;
}
.nav-links{
   display:flex;
}
.pagination .page-numbers{
   display:inline-block;
   margin-right:20px;
   padding:20px 25px;
   color:#333;
   border-radius:3px;
   box-shadow:0 3px 3px #999;
   background:#fff;
}
.pagination .current{
   padding:20px 25px;
   background:#081535;
   color:#fff;
}
.pagination .prev,
.pagination .next{
   background:transparent;
   box-shadow:none;
   color:#081535;
}
.pagination .dots{
   background:transparent;
   box-shadow:none;
}
@media only screen and (max-width: 767px){
  .pagination .page-numbers{
    display: none;
  }

  .pagination .prev,
  .pagination .next{
     display:inline-block;
     margin-right:20px;
     padding:20px 25px;
     color:#333;
     border-radius:3px;
     box-shadow:0 3px 3px #999;
     background:#fff;
  }
}
</style>
<ul class="penci-wrapper-data penci-grid">
<?php
// main content loop
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
  'post_status' => 'publish',
  'post_type' => 'post', //カスタム投稿タイプを指定
  'category_name' => 'present', //カスタムタクソノミーを指定
  'posts_per_page' => 12, //表示する記事数
  'paged' => $paged,
  'orderby'     => 'date',
  'order' => 'DESC'
);
$news_query = new WP_Query( $args ); //サブループを変数に格納
if ( $news_query->have_posts() ) : 
  while ( $news_query->have_posts() ) : 
    $news_query->the_post(); 
	
	// 日付判断（応募期限判断）
	$due_date = get_post_meta($post->ID , 'due_date' ,true);
	$isGray = "gray-img ";
	$isFinish = '<span class="due-date">応募期間終了</span>';
	if( $due_date ){
		if( strtotime(date("Y/m/d H:i:s")) < strtotime($due_date) ){
			$isGray = "";
			$isFinish = "";
		}
	}
?>
<li class="grid-style">
	<article id="post-5226" class="item hentry">
		<div class="thumbnail">
			<a class="<?=$isGray?>penci-image-holder penci-lazy" data-src="<?php the_post_thumbnail_url(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			</a>
		</div>
		<div class="grid-header-box">
			<h2 class="penci-entry-title entry-title grid-title"><a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<?=$isFinish?>
		</div>
	</article>
</li>
<?php
endwhile;
endif;
// loop end
?>
</ul>
<div class="pagination">
   <div class="list-box">
       <ul>
       <?php
       $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
       if ($news_query->have_posts()) :
           while ($news_query->have_posts()) : $news_query->the_post();
           ?>
               <?php
               /*　ここにループさせるコンテンツを入れます　*/
               ?>
           <?php
           endwhile;
       else:
           echo '<div><p>プレゼント情報がありません</p></div>';
       endif;
       ?>
       </ul>
   </div>
 
   <div class="pnavi">
       <?php //ページリスト表示処理
       global $wp_rewrite;
       $paginate_base = get_pagenum_link(1);
       if(strpos($paginate_base, '?') || !$wp_rewrite->using_permalinks()){
           $paginate_format = '';
           $paginate_base = add_query_arg('paged','%#%');
       }else{
           $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
           user_trailingslashit('page/%#%/','paged');
           $paginate_base .= '%_%';
       }
       echo paginate_links(array(
           'base' => $paginate_base,
           'format' => $paginate_format,
           'total' => $news_query->max_num_pages,
           'mid_size' => 1,
           'current' => ($paged ? $paged : 1),
           'prev_text' => '< 前へ',
           'next_text' => '次へ >',
       ));
       ?>
   </div>
</div>
<?php
wp_reset_postdata();
?>
				<?php endwhile; endif; ?>
            </div>
        </div>

		<?php if ( in_array( $sidebar_default, array( 'left-sidebar', 'right-sidebar', 'two-sidebar' ) ) ) : ?>
			<?php get_sidebar(); ?>
		<?php endif; ?>
		<?php if ( $sidebar_default == 'two-sidebar' ) : get_sidebar( 'left' ); endif; ?>
</div>
<?php get_footer(); ?>
