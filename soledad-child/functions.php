<?php
/*
 Soledad child theme functions and definitions
*/
function penci_soledad_child_scripts() {
    wp_enqueue_style( 'penci-soledad-parent-style', get_template_directory_uri(). '/style.css' );
	if ( is_rtl() ) {
		wp_enqueue_style( 'penci-soledad-rtl-style', get_template_directory_uri(). '/rtl.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'penci_soledad_child_scripts', 100 );

/*
 * WordPress管理画面の投稿一覧【ユーザー】絞り込み
 */

function add_author_filter() {
global $post_type;
if ( $post_type == 'post' ) {
wp_dropdown_users( array(
'show_option_all' => 'ユーザー一覧',
'name' => 'author'
) );
}
}
add_action( 'restrict_manage_posts', 'add_author_filter' );

//カテゴリスラッグクラスをbodyクラスに含める
function add_category_slug_classes_to_body_classes($classes) {
  global $post;
  if ( is_single() ) {
    foreach((get_the_category($post->ID)) as $category)
       $classes[] = $category->category_nicename;
  }
  return $classes;
}
add_filter('body_class', 'add_category_slug_classes_to_body_classes');

function permalink_change_post( $permalink, $post, $leavename ) {
 
    // 記事が属するカテゴリと親カテゴリを取得
    $category = get_the_category($post->ID);
    $parent = get_category($category[0]->parent);
     
    // 親カテゴリが存在する場合は、フラットなURLにカスタマイズ
    if ( empty($category[0]->parent) === false )
    {
        $permalink = home_url('/'.$category[0]->slug.'/'.$post->post_name.'.html' );
    }
 
    return $permalink;
}
 
add_filter( 'post_link', 'permalink_change_post', 10, 3 );

function permalink_change_category( $permalink, $post ) {
 
    // パーマリンクにnetshopという文字列が含まれる場合、その部分を削除してURLをフラット化
    if( strpos($permalink, 'netshop') )
    {
        $permalink = str_replace(array("netshop/"), '', $permalink);
    }
 
    return $permalink;
}
 
add_filter( 'category_link', 'permalink_change_category', 11, 2 );