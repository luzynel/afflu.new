<?php get_header(); ?>

<div class="penci-single-wrapper">
	<div class="penci-single-block">
		<div class="penci-single-pheader container penci_sidebar two-sidebar single-present-page">
			
			<div class="wrapper-pad ">
						<?php
						$yoast_breadcrumb = '';
						if ( function_exists( 'yoast_breadcrumb' ) ) {
							$yoast_breadcrumb = yoast_breadcrumb( '<div class="container penci-breadcrumb penci-crumb-inside single-breadcrumb">', '</div>', false );
						}

						if( $yoast_breadcrumb ){
							echo $yoast_breadcrumb;
						}else{
							$yoast_breadcrumb = '';
							if ( function_exists( 'yoast_breadcrumb' ) ) {
								$yoast_breadcrumb = yoast_breadcrumb( '<div class="container penci-breadcrumb penci-crumb-inside single-breadcrumb">', '</div>', false );
							}

							if( $yoast_breadcrumb ){
								echo $yoast_breadcrumb;
							 }else{ ?>
							<div class="container penci-breadcrumb penci-crumb-inside single-breadcrumb">
								<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>
								<?php
								if ( get_theme_mod( 'enable_pri_cat_yoast_seo' ) ) {
									$primary_term = penci_get_wpseo_primary_term();

									if ( $primary_term ) {
										echo $primary_term;
									} else {
										$penci_cats = get_the_category( get_the_ID() );
										$penci_cat  = array_shift( $penci_cats );
										echo penci_get_category_parents( $penci_cat );
									}
								} else {
									$penci_cats = get_the_category( get_the_ID() );
									$penci_cat  = array_shift( $penci_cats );
									echo penci_get_category_parents( $penci_cat );
								}
								?>
								<span><?php the_title(); ?></span>
							</div>
							<?php } ?>
						<?php } ?>
					
				<h1 class="post-title single-post-title entry-title">
					<?php the_title(); ?>
				</h1>
				<div class="date"><span>ISSUED | <?php echo get_the_date( 'Y', get_the_ID() ); ?>.
	            <?php echo get_the_date( 'F', get_the_ID() ); ?></span></div>

	            <?php
	            	$image = get_field('present_main', get_the_ID());
	            	$present_main_cap = get_field('present_main_cap', get_the_ID());
	            	$present_sub_ttl = get_field('present_sub_ttl', get_the_ID());
	            	$present_winner = get_field('present_winner', get_the_ID());
	            	$present_page = get_field('present_page', get_the_ID());
	            ?>
	            
	            <div class="present_main">
	            	<?php
	            		if( !empty($image) ):
							echo '<img src="'. $image .'" alt="present_image" style="width:100%"/>';
						endif;
	            	?>
	            </div>
	            <div class="present_main_cap">
	            	<?php 
		            	if( !empty($present_main_cap) ):
							echo $present_main_cap;
						endif;
		            ?>
	            </div>

	            <div class="present_sub_ttl">
		            <?php 
		            	if( !empty($present_sub_ttl) ):
							echo '<h2 class="pre-sub_ttl">'. $present_sub_ttl . '</h2>';
						endif;
		            ?>
		            <?php 
		            	if( !empty($present_winner) ):
							echo '<h2 class="pre-winner">'. $present_winner . '</h2>';
						endif;
		            ?>
	            </div>
			</div>
			
	    	<div id="content" class="penci-single-pheader container container-single two-sidebar container-single-fullwidth hentry">
	            <?php the_content(); ?>
			</div>

			<div class="present_main_cap btn_wrp">
	        	<?php if( !empty($present_page) ): ?>
					<a href="<?php echo $present_page; ?>" target="_blank"><span>応募ページへ</span></a>	
	            <?php endif; ?>
	        </div>

	    </div>
	</div>
</div>

<?php get_footer(); ?>